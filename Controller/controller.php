<?php

require_once("DBModel/DBModel.php");

class Controller{
	
	protected $model;
	
	public function __construct(){
		$this->model = new Model();
	}
	
	public function invoke(){
		$this->model->main();
	}
}
?>
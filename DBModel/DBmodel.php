<?php
class Model{
	
	public $dom;
	protected $db;
	
	
	public function __construct(){
		try{
			$this->dom = new DOMDocument();
			$this->dom->load('DBModel/Skierlogs.xml');
			$this->db = new PDO('mysql:host=localhost; dbname=skierlogs2; charset=utf8', 'root', '');
			$this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		} catch(Exception $e) {
			echo "Error accured creatingnew PDO..";
			echo $e->getMessage();
		}
	}
	
	
	public function main(){
		
		$this->restoreTotalDist();
		$this->club();
		$this->skiers();
		$this->seasons();
		
		
	}
	
	public function club(){
		$clubId = array();
		$names = array();
		$citys = array();
		$countys =array();
		
		$clubs = $this->dom->getElementsByTagName("Club");
		$name = $this->dom->getElementsByTagName("Name");
		$city = $this->dom->getElementsByTagName("City");
		$county = $this->dom->getElementsByTagName("County");
		
		
		foreach($clubs as $club){
			echo '<pre>';
			array_push($clubId, $club->getAttribute('id'));
		}
		
		foreach($name as $n){
			echo '<pre>';
			array_push($names, $n->textContent);
		}
		
		foreach($city as $c){
			echo '<pre>';
			array_push($citys, $c->textContent);
		}
		
		foreach($county as $co){
			echo '<pre>';
			array_push($countys, $co->textContent);
		}
		
		for($i=0;$i<sizeof($countys);$i++){
			try{
				$query=$this->db->prepare('INSERT INTO clubs (clubId, name, city, county) VALUES(:clubId, :name, :city, :county)');
				$query->bindValue(':clubId', $clubId[$i]);
				$query->bindValue(':name', $names[$i]);
				$query->bindValue(':city', $citys[$i]);
				$query->bindValue(':county', $countys[$i]);
				$query->execute();
						
			}catch(Exception $e){
				echo "Something went wrong";
				$e->getMessage();
			}
		}
	}
	
	public function skiers(){
		$userName = array();
		$firstName = array();
		$lastName = array();
		$yearOfBirth =array();
		
		$uName = $this->dom->getElementsByTagName("Skier");
		$fName = $this->dom->getElementsByTagName("FirstName");
		$lName = $this->dom->getElementsByTagName("LastName");
		$yob = $this->dom->getElementsByTagName("YearOfBirth");
		
		
		foreach($uName as $un){
			echo '<pre>';
			array_push($userName, $un->getAttribute('userName'));
		}
		$userName = array_unique($userName);
		
		foreach($fName as $fn){
			echo '<pre>';
			array_push($firstName, $fn->textContent);
		}
		
		foreach($lName as $ln){
			echo '<pre>';
			array_push($lastName, $ln->textContent);
		}
		
		foreach($yob as $yOB){
			echo '<pre>';
			array_push($yearOfBirth, $yOB->textContent);
		}
		
		for($i=0;$i<sizeof($firstName);$i++){
			try{
				$query=$this->db->prepare('INSERT INTO skiers (userName, firstName, lastName, yearOfBirth) VALUES(:uName, :fName, :lName, :yob)');
				$query->bindValue(':uName', $userName[$i]);
				$query->bindValue(':fName', $firstName[$i]);
				$query->bindValue(':lName', $lastName[$i]);
				$query->bindValue(':yob', $yearOfBirth[$i]);
				$query->execute();
						
			}catch(Exception $e){
				echo "Something went wrong<br>";
				$e->getMessage();
			}
		}
		
	}
	
	public function seasons(){
		
		$fallYear = array();
		
		$season = $this->dom->getElementsByTagName("Season");
		
		foreach($season as $s){
			echo '<pre>';
			array_push($fallYear, $s->getAttribute('fallYear'));
		}
		
		for($i=0;$i<sizeof($fallYear);$i++){
			try{
				$query=$this->db->prepare('INSERT INTO seasons (fallYear) VALUES(:fallYear)');
				$query->bindValue(':fallYear', $fallYear[$i]);
				$query->execute();
			}
			
			catch(Exception $e){
				echo "Something went wrong<br>";
				$e->getMessage();
			}
		
		}
	}

	public function restoreTotalDist(){
		$uName = array();
        $userSea16 = array();
        $distance_15 = array();
        $distance_16 = array();

        $xpath= new DOMXPath($this->dom);


        $usernames_15 = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
        foreach($usernames_15 as $user){
            array_push($uName, trim($user->textContent));
        }

        $sum = 0;
        for($i = 0; $i< sizeof($uName); $i++){
            $dist = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$uName[$i].'"]/Log/Entry/Distance');
            for($j = 0; $j <$dist->length; $j++){
                $sum += ($dist->item($j)->textContent);
            }
            array_push($distance_15, $sum);
            $sum = 0;
        }

        $usernames_16 = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
        foreach($usernames_16 as $user){
            array_push($userSea16, trim($user->textContent));
        }

        $sum = 0;
        for($i = 0; $i< sizeof($userSea16); $i++){
            $dist_2 = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$userSea16[$i].'"]/Log/Entry/Distance');
            for($j = 0; $j <$dist_2->length; $j++){
                $sum += ($dist_2->item($j)->textContent);
            }
            array_push($distance_16, $sum);
            $sum = 0;
        }


        $ses = $xpath->query('//SkierLogs/Season/@fallYear');

        for($i = 0; $i< sizeof($uName); $i++){
            try {
                $null = NULL;
                $club = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$uName[$i].'"]/ancestor::Skiers/@clubId');
                $input = $this->db->prepare("INSERT INTO totaldistancelog(sUserName, sFallYear, cClubId, totalDistance) VALUES (:user, :fall, :club, :tot_dist)");
                $input->bindValue(':user', $uName[$i], PDO::PARAM_STR);
                $input->bindValue(':fall', $ses->item(0)->textContent, PDO::PARAM_STR);
                $input->bindValue(':tot_dist', $distance_15[$i], PDO::PARAM_STR);
                if($club->length){
                    $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
                }else $input->bindValue(':club', $null, PDO::PARAM_STR);
                $input->execute();
            } catch (Exception $e) {
                echo "Failed to update totaldistancelog table in db!".'<br>';
                echo $e->getMessage();
            }

        }

        for($i = 0; $i< sizeof($userSea16); $i++){
            try {
                $null = NULL;
                $club = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$userSea16[$i].'"]/ancestor::Skiers/@clubId');
                $input = $this->db->prepare("INSERT INTO totaldistancelog(sUserName, sFallYear, cClubId, totalDistance) VALUES (:user, :fall, :club, :tot_dist)");
                $input->bindValue(':user', $userSea16[$i], PDO::PARAM_STR);
                $input->bindValue(':fall', $ses->item(1)->textContent, PDO::PARAM_STR);
                $input->bindValue(':tot_dist', $distance_16[$i], PDO::PARAM_STR);
                if($club->length){
                    $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
                }else $input->bindValue(':club', $null, PDO::PARAM_STR);
                $input->execute();
            } catch (Exception $e) {
                echo "Failed to update totaldistancelog table in db!".'<br>';
                echo $e->getMessage();
            }
        }
    }


}
	
?>
